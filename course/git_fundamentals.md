---
title: Git Fundamentals
nav_order: 100
---

# Git Fundamentals

![git logo](https://i.imgur.com/DJEpvCv.png)

git is a *free* and *open source* distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

Git is easy to learn and has a tiny footprint with lightning fast performance. It outclasses SCM tools like **Subversion**, **CVS**, **Perforce**, and **ClearCase** with features like cheap local branching, convenient staging areas, and multiple workflows.

## Useful links

- [About](https://git-scm.com/about)
- [Documentation](https://git-scm.com/doc)
- [Downloads](https://git-scm.com/downloads)
- [Community](https://git-scm.com/community)

## Useful commands
Some useful commands are:

1. `git add` to add new or changed files to the staging area
2. `git commit` to capture those changes
3. `git push` to send them out to a remote repository!


{% attach_file {"file_name": "pdf/pro_git.pdf", "title":"Click here to download the \"Pro Git book\""} %}

{% attach_file {"file_name": "slides_markdown/git_slides.md", "title":"Git slide deck"} %}